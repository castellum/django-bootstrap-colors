# 5.3.0 (2023-06-12)

-   Compatible with bootstrap 5.3


# 5.2.2 (2023-05-23)

-   do not return invalid CSS on missing setting


# 5.2.1 (2023-05-23)

-   do not raise exception on missing setting


# 5.2.0 (2023-05-16)

initial release
